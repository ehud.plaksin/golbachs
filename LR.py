import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np

def vec_bin_array(arr, m):
    """
    Arguments:
    arr: Numpy array of positive integers
    m: Number of bits of each integer to retain

    Returns a copy of arr with every element replaced with a bit vector.
    Bits encoded as int8's.
    """
    to_str_func = np.vectorize(lambda x: np.binary_repr(x).zfill(m))
    strs = to_str_func(arr)
    ret = np.zeros(list(arr.shape) + [m], dtype=np.int8)
    for bit_ix in range(0, m):
        fetch_bit_func = np.vectorize(lambda x: x[bit_ix] == '1')
        ret[...,bit_ix] = fetch_bit_func(strs).astype("int8")

    return ret

def get_mod3_mod5_mod7(arr):
    """
    :param arr: array of positive integers
    :return: copy of arr with appended colums of,
    the one hot encoded resediue in mod3 , mod 5 and mod 7
    """
    # arr = [[divmod(x/2,3)[1] == 0,divmod(x,5)[1],divmod(x,7)[1],divmod(x,11)[1],divmod(x,13)[1],divmod(x,17)[1]] for x in arr]
    R_3 = np.zeros([len(arr), 3])
    R_5 = np.zeros([len(arr), 5])
    R_7 = np.zeros([len(arr), 7])
    R_11 = np.zeros([len(arr), 11])
    R_13 = np.zeros([len(arr), 13])
    for i in range(0, len(arr)):
        r_3 = divmod(arr[i],3)
        if(r_3[1] == 0):
            R_3[i] = [0,0,1]
        if (r_3[1] == 1):
            R_3[i] = [0, 1, 0]
        if (r_3[1] == 2):
            R_3[i] = [1, 0, 0]

        r_5 = divmod(arr[i], 5)
        if (r_5[1] == 0):
            R_5[i] = [0, 0, 0, 0, 1]
        if (r_5[1] == 1):
            R_5[i] = [0, 0, 0, 1, 0]
        if (r_5[1] == 2):
            R_5[i] = [0, 0, 1, 0, 0]
        if (r_5[1] == 3):
            R_5[i] = [0, 1, 0, 0, 0]
        if (r_5[1] == 4):
            R_5[i] = [1, 0, 0, 0, 0]

        r_7 = divmod(arr[i], 7)
        if (r_7[1] == 0):
            R_7[i] = [0, 0, 0, 0, 0, 0, 1]
        if (r_7[1] == 1):
            R_7[i] = [0, 0, 0, 0, 0, 1, 0]
        if (r_7[1] == 2):
            R_7[i] = [0, 0, 0, 0, 1, 0, 0]
        if (r_7[1] == 3):
            R_7[i] = [0, 0, 0, 1, 0, 0, 0]
        if (r_7[1] == 4):
            R_7[i] = [0, 0, 1, 0, 0, 0, 0]
        if (r_7[1] == 5):
            R_7[i] = [0, 1, 0, 0, 0, 0, 0]
        if (r_7[1] == 6):
            R_7[i] = [1, 0, 0, 0, 0, 0, 0]

        r_11 = divmod(arr[i], 11)
        if (r_11[1] == 0):
            R_11[i] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]
        if (r_11[1] == 1):
            R_11[i] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0]
        if (r_11[1] == 2):
            R_11[i] = [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0]
        if (r_11[1] == 3):
            R_11[i] = [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0]
        if (r_11[1] == 4):
            R_11[i] = [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0]
        if (r_11[1] == 5):
            R_11[i] = [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0]
        if (r_11[1] == 6):
            R_11[i] = [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0]
        if (r_11[1] == 7):
            R_11[i] = [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
        if (r_11[1] == 8):
            R_11[i] = [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
        if (r_11[1] == 9):
            R_11[i] = [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        if (r_11[1] == 10):
            R_11[i] = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    # r_13 = divmod(arr[i], 13)
    # if (r_13[1] == 0):
    #     R_13[i] = [0,0,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]
    # if (r_13[1] == 1):
    #     R_13[i] = [0,0,0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0]
    # if (r_13[1] == 2):
    #     R_13[i] = [0,0,0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0]
    # if (r_13[1] == 3):
    #     R_13[i] = [0,0,0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0]
    # if (r_13[1] == 4):
    #     R_13[i] = [0,0,0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0]
    # if (r_13[1] == 5):
    #     R_13[i] = [0,0,0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0]
    # if (r_13[1] == 6):
    #     R_13[i] = [0,0,0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0]
    # if (r_13[1] == 7):
    #     R_13[i] = [0,0,0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
    # if (r_13[1] == 8):
    #     R_13[i] = [0,0,0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
    # if (r_13[1] == 9):
    #     R_13[i] = [0,0,0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    # if (r_13[1] == 10):
    #     R_13[i] = [0,0,1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    # if (r_13[1] == 11):
    #     R_13[i] = [0,1,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    # if (r_13[1] == 12):
    #     R_13[i] = [1,0,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    return np.c_[np.c_[np.c_[R_3,R_5],R_7],R_11]

def append_bias_reshape(features,labels):
    n_training_samples = features.shape[0]
    n_dim = features.shape[1]
    f = np.reshape(np.c_[np.ones(n_training_samples),features],[n_training_samples,n_dim + 1])
    l = np.reshape(labels,[n_training_samples,1])
    return f, l

BUS_WIDTH = 16;
display_step = 100
learning_rate = 0.01
training_epochs = 1000
beta = 0.005
batch_size = 2048

XY = np.loadtxt('/home/ehud/PycharmProjects/Goldbach/sdataset.txt',dtype = 'int')
np.random.shuffle(XY)
f = XY[:, 0]
l = XY[:, 1]
#
mods = get_mod3_mod5_mod7(f)
f = vec_bin_array(f,BUS_WIDTH)
f = np.c_[f,mods]
f, l = append_bias_reshape(f,l)
n_dim = f.shape[1]

rnd_indices = np.random.rand(len(f)) < 0.70
train_x = f[rnd_indices]
train_y = l[rnd_indices]
test_x = f[~rnd_indices]
test_y = l[~rnd_indices]
fet = f = XY[:, 0]
fet = fet[~rnd_indices]

cost_history = np.empty(shape=[1],dtype=float)

X = tf.placeholder(tf.float32,[None,n_dim])
Y = tf.placeholder(tf.float32,[None,1])
# W = tf.Variable(tf.ones([n_dim,1]))

weight_initer = tf.truncated_normal_initializer(mean=0.0, stddev=0.01)
W = tf.get_variable(name="Weight", dtype=tf.float32, shape=[n_dim,1], initializer=weight_initer)

init = tf.global_variables_initializer()

regularizer = tf.nn.l2_loss(W)
y_ = tf.matmul(X, W)
cost = tf.reduce_mean(tf.square(y_ - Y) + beta * regularizer)
# cost = tf.reduce_mean(tf.square(y_ - Y) )

training_step = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)
with tf.Session() as sess:
    sess.run(init)
    for epoch in range(training_epochs):

        offset = (epoch * batch_size) % (train_x.shape[0] - batch_size)
        batch_data = train_x[offset:(offset + batch_size), :]
        batch_labels = train_y[offset:(offset + batch_size), :]

        sess.run(training_step, feed_dict={X: batch_data, Y: batch_labels})
        cost_history = np.append(cost_history, sess.run(cost, feed_dict={X: batch_data, Y: batch_labels}))


        if (epoch + 1) % display_step == 0:
            c = sess.run(cost, feed_dict={X: batch_data, Y: batch_labels})
            print("Epoch:", '%04d' % (epoch + 1), "cost=", "{:.9f}".format(c), "W=", sess.run(W))

    # calculate mean square error
    pred_y = sess.run(y_, feed_dict={X: test_x})
    mse = tf.reduce_mean(tf.square(pred_y - test_y))
    mse = sess.run(mse)
    print("MSE: %.4f" % mse,"RMSE: %.4f" % np.sqrt(mse))
    # plot cost
    plt.plot(range(len(cost_history)), cost_history)
    plt.axis([0, training_epochs, 0, np.max(cost_history)])
    plt.show()

    fig, ax = plt.subplots()
    ax.scatter(test_y, pred_y, s = 5)
    ax.plot([test_y.min(), test_y.max()], [test_y.min(), test_y.max()], 'k--', lw=3)
    ax.set_xlabel('Measured')
    ax.set_ylabel('Predicted')
    plt.show()

    fig, ax = plt.subplots()
    ax.scatter(fet, pred_y, s=5)
    ax.set_xlabel('X')
    ax.set_ylabel('G(X)')
    plt.show()